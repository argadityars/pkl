# PKL

## URL Route

1. `GET` satsetbatbet.com/pkl/api/logs

```
Digunakan untuk melihat seluruh data log
```

2. `POST` satsetbatbet.com/pkl/api/logs

```
Digunakan untuk insert data baru ke database
Data:
network_type, post_type, packetloss
```
