<?php
require_once __DIR__ . '/vendor/autoload.php';
use RedBeanPHP\Facade as R;
use Siler\Dotenv;
use Siler\Route;

Dotenv\init(__DIR__);
R::setup('mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DBNAME'), getenv('MYSQL_USER'), getenv('MYSQL_PASS'));

Route\get('/', 'controller/pages/index.php');
Route\resource('/api/logs', 'controller/logs');
