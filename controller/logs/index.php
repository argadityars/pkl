<?php
use RedBeanPHP\Facade as R;
use Siler\Http\Response;

$logs = R::findAll('logs');
Response\json([
    'error' => false,
    'message' => 'Data retrieved successfully',
    'data' => array_values($logs),
]);
