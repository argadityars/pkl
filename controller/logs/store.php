<?php
use RedBeanPHP\Facade as R;
use Siler\Http\Request;
use Siler\Http\Response;

$input_length = count(Request\post());
$logs_length = count(R::getAll('SHOW COLUMNS FROM logs')) - 1;
if ($input_length === $logs_length) {
    $log = R::dispense('logs');
    foreach (Request\post() as $key => $value) {
        $log[$key] = $value;
    }
    $logId = R::store($log);

    Response\json([
        'success' => true,
        'message' => 'Data stored successfully',
        'data' => R::load('logs', $logId),
    ]);
} else {
    Response\header('status', '422 Unprocessable Entity');
    Response\json([
        'success' => false,
        'message' => 'Data input is not valid',
        'data' => null,
    ]);
}
